import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStreamReader;

public class ConsoleConnection implements Runnable {

    DataInputStream inputStream;
    private volatile boolean running;
    public ConsoleConnection(){
        inputStream = new DataInputStream(System.in);
    }
    public void run(){
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(inputStream));
        while(running = true){
            try {
                String line = stdIn.readLine();
                if(line.equals("exit")){
                    System.out.println("Terminate server...");
                    System.exit(1);
                }
            } catch(Exception e){
                e.printStackTrace();
            }

        }


    }
}
