import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ServerConnection {

    private ServerSocket ss;
    private List<ServerSideConnection> playersConnections = new ArrayList<>();
    private static int numPlayer;

    public ServerConnection() {
        System.out.println("____Game Server____");
        numPlayer = 0;
        try {
            ss = new ServerSocket(51734);
        } catch (IOException ex) {
            System.out.println("IO Exception");
        }
    }

    public void acceptConnections() {
        try {
            System.out.println("Waiting for connections...");
            Thread consoleThread = new Thread(new ConsoleConnection());
            consoleThread.start();
            while (numPlayer < 10) {
                Socket s = ss.accept();
                numPlayer++;
                System.out.println("Player #" + numPlayer + " has connected.");
                ServerSideConnection serverSideConnection = new ServerSideConnection(s, numPlayer);
                playersConnections.add(serverSideConnection);
                Thread t = new Thread(serverSideConnection);
                t.start();

            }
            System.out.println(numPlayer + " Players are now connected.");
        } catch (IOException ex) {
            System.out.println("An IO Exception occurred while trying to connect to the server.");
        }

    }

}

