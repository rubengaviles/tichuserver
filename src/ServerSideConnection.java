import java.io.*;
import java.net.Socket;

public class ServerSideConnection implements Runnable {
    private Socket socket;
    private DataInputStream dis;
    private DataOutputStream dos;
    private int playerID;
    private BufferedReader bufferedReader;
    private boolean connected;

    public ServerSideConnection(Socket s, int playerID){
        this.socket = s;
        this.playerID = playerID;
        try {
            dis = new DataInputStream(socket.getInputStream());
            dos = new DataOutputStream(socket.getOutputStream());
            bufferedReader = new BufferedReader(new InputStreamReader(dis));
            connected = true;
        } catch(IOException e){
            System.out.println("IOException from ServerSideConnection Constructor");
        }

    }

    public void run() {
        while (connected) {
            try {
                dos.writeInt(playerID);
                dos.flush();
                String request = bufferedReader.readLine();
                if (request.equals("playerExit")) {
                    System.out.println("Player "+ playerID + " has disconnected");
                    connected = false;
                }

            } catch (IOException ex) {
                System.out.println("IOException from SSC run()");
            }
        }
    }

}
